FROM phpunit/phpunit:9.1.1

COPY . /white_rabbit

ENTRYPOINT phpunit /white_rabbit/test