<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use PHPUnit\Framework\TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp(): void
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

    public function multiplyProvider(){
        return array(
            array(4, 2, 2),
            array(6, 3, 2),

            /**
             * Following are my test cases to show that the method "multiplyBy" is flawed
             */

            array(0.49, 1, 0.49), // Method fails when using float values, since it rounds up the returned amount
            array(0, 7, 0), // Method fails when 7 is used amount, as $guess value becomes 0, i.e. $guess = abs(7 - 7)
            array(49, 7, 7), // Method fails when 7 is used in both amount & multiplier, as explained above
            array(49.7, 7.1, 7), // Also "round($amount)" fails as it returns 49. Expected was 49.7, although wrong it should return 50 since "round" is being used
            array(28.6, 5.2, 5.5), // Also 'round($amount)' fails as it returns 29. Expected was 29.7, although wrong it should return 30 since 'round' is being used
        );
        /**
         * In general, the method fails:
         * if float values are used in amount and/or multiplier.
         * if the amount is set to 7 (except in case multiplier is set to 1)
		 *
         * And in some cases, however the result is wrong, "round" also fails in the method
		 * where the integer part (left part of the decimal) of amount and multiplier is an odd number
         * and the difference between amount and multiplier is either (0.1, 0.3, 0.5,..)
         * Eg. See the tests on lines 42 and 43
         */
    }
}
