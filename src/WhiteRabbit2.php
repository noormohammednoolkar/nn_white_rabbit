<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        /*
        if($amount == 26){
            return array(
                '1'   => 1,
                '2'   => 0,
                '5'   => 1,
                '10'  => 0,
                '20'  => 1,
                '50'  => 0,
                '100' => 0
            );
        }*/

        /**
         * Initializing the coin/return array
         */

        $coinArr = array(
            '1' => 0,
            '2' => 0,
            '5' => 0,
            '10' => 0,
            '20' => 0,
            '50' => 0,
            '100' => 0
        );

        if (is_int($amount) === true and $amount > 0) {
            /**
             * Obviously the amount should be greater than 0 to calculate number of coins
             */

            /**
             * To find least number of coins required, we have to start with the highest coin value
             */

            $revCA = array_reverse($coinArr, true);

            /**
             * Logic: Divide the amount with highest value coin,
             * Quotient  = Number of coins of highest value
             * Remainder = Remaining amount value to check against next highest coin
             * We loop until the array exhausts/amount and gives us the coin/return array
             */

            foreach ($revCA as $key => $val) {
                if (($coins = intval($amount / $key)) > 0) {
                    $coinArr[$key] = $coins;
                    $amount = $amount % $key;
                }

                if (intval($amount) <= 0) {
                    break;
                }
            }
        }

        return $coinArr;
    }
}